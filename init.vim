"Neovim specific start
"set termguicolors
"set encoding=utf8
"let $NVIM_TUI_ENABLE_CURSOR_SHAPE=1
"let $NVIM_PYTHON_LOG_FILE="~/.nvim_python_log"
"let g:airline_powerline_fonts=1
"set guifont=Droid\ Sans\ Mono\ for\ Powerline\ Plus\ Nerd\ File\ Types\ 11

set nocompatible
filetype plugin indent on
set runtimepath^=~/.vim/dein/repos/github.com/Shougo/dein.vim

call dein#begin(expand('~/.vim/dein'))
call dein#add('Shougo/dein.vim')
call dein#add('Shougo/vimproc.vim', {
    \   'build': {
    \       'windows': 'tools\\update-dll-mingw',
    \       'cygwin': 'make -f make_cygwin.mak',
    \       'mac': 'make -f make_mac.mak',
    \       'linux': 'make',
    \       'unix': 'gmake',
    \       }
    \   })


"call dein#add('ryanoasis/vim-devicons')
call dein#add('Shougo/unite.vim')
call dein#add('Shougo/neosnippet.vim')
call dein#add('Shougo/neosnippet-snippets')
call dein#add('Shougo/deoplete.nvim',
    \{'on_i': 1})
call dein#add('neovim/node-host')
"call dein#add('snoe/nvim-parinfer.js')    
call dein#add('tpope/vim-salve')
call dein#add('tpope/vim-projectionist')
call dein#add('tpope/vim-dispatch')
call dein#add('tpope/vim-sexp-mappings-for-regular-people')
call dein#add('guns/vim-sexp')
call dein#add('tpope/vim-repeat')
call dein#add('tpope/vim-surround')
call dein#add('kien/rainbow_parentheses.vim')
call dein#add('guns/vim-clojure-static')
call dein#add('guns/vim-clojure-highlight')
call dein#add('tpope/vim-fireplace')
call dein#add('mattn/emmet-vim')
"call dein#add('markwoodhall/vim-figwheel')
"call dein#add('vim-airline/vim-airline')
"call dein#add('vim-airline/vim-airline-themes')

call dein#end()

if dein#check_install()
    call dein#install()
endif

"call dein#update()

call dein#remote_plugins()

"let g:parinfer_mode = "indent"

"Neovim specific end

"Unite settings
let g:unite_source_history_yank_enable = 1
call unite#filters#matcher_default#use(['matcher_fuzzy'])
nnoremap ;t :<C-u>Unite -no-split -buffer-name=files    file_rec/async:!<cr>
nnoremap ;ts :<C-u>Unite -buffer-name=files    file_rec/async:!<cr>
nnoremap ;tsv :<C-u>Unite -vertical -buffer-name=files    file_rec/async:!<cr>
nnoremap ;f :<C-u>Unite -no-split -buffer-name=files    file<cr>
nnoremap ;fs :<C-u>Unite -buffer-name=files   file<cr>
nnoremap ;fsv :<C-u>Unite -vertical -buffer-name=files   file<cr>
nnoremap ;r :<C-u>Unite -no-split -buffer-name=mru     file_mru<cr>
nnoremap ;o :<C-u>Unite -no-split -buffer-name=outline outline<cr>
"nnoremap ;y :<C-u>Unite -no-split -buffer-name=yank    history/yank<cr>
nnoremap ;y :<C-u>Unite -buffer-name=yank    history/yank<cr>
"nnoremap ;b :<C-u>Unite -no-split -buffer-name=buffer  buffer<cr>
nnoremap ;b :<C-u>Unite -buffer-name=buffer  buffer<cr>

" Custom mappings for the unite buffer
autocmd FileType unite call s:unite_settings()
function! s:unite_settings()
  " Play nice with supertab
    let b:SuperTabDisabled=1
    " Enable navigation with control-j and control-k in insert mode
    imap <buffer> <C-j>   <Plug>(unite_select_next_line)
    imap <buffer> <C-k>   <Plug>(unite_select_previous_line)
endfunction"

let g:salve_auto_start_repl = 1

syntax on
set nu
set ai
set noexpandtab
set tabstop=4
set shiftwidth=4
set expandtab
"set ic
set laststatus=2
set showmatch
set ruler
set history=100
"Unlimited backspace in insert mode
set backspace=indent,eol,start
set showcmd
set autowrite
set hidden
"change the terminal so we know what file is being edited
"autocmd BufEnter * let &titlestring = expand("%:t%M") . "(" . hostname() . ")"
"set titlestring=%t%(\ %M%)%(\ (%{expand(\"%:p:h\")})%)%(\ %a%)\ -\ %{v:servername}
set titlestring=%t%(\ %M%)%(\ (%{hostname(\)\})%) 
set title
set foldmethod=manual
"colorscheme fine_blue
"colorscheme gaea
"colorscheme sorcerer
"colorscheme spring
"colorscheme darkblue
colorscheme vibrantink
"colorscheme vividchalk
colorscheme wombat256
"colorscheme wombat256mod
"fix visual background color
hi Visual term=reverse cterm=reverse
"guibg=LightGrey
let mapleader=","
"TODO forward from current position 1/4 of a line
"nnoremap <leader> :exe 'normal '.(virtcol('$')/4).'|'
"ii to exit insert mode 
inoremap <silent> ii <Esc>:let &insertmode=0<CR>
tnoremap <silent> ii <C-\><C-n> 
"toggle showing hidden characters
nnoremap <leader>l :set list!<CR>
"set some laptop convenience keys - mainly because the r-ctl key is
"  positioned badly
"nnoremap <leader>d <C-d>
nnoremap <leader>w <C-w>w
nnoremap <leader>p <C-w><C-p>
nmap <C-j> <C-w>j
nmap <C-k> <C-w>k
nmap <C-h> <C-w>h
nmap <C-l> <C-w>l
"resize current buffer by +/- 5 
nnoremap <leader>s :vertical resize -5<cr>
nnoremap <leader>d :resize +5<cr>
nnoremap <leader>e :resize -5<cr>
nnoremap <leader>f :vertical resize +5<cr>
"edit the init.vim
nnoremap <leader>ei :e $MYVIMRC<cr>
"reload the init.vim
nnoremap <leader>si :so $MYVIMRC<cr>
"refresh syntax highlighting from start of file
nnoremap <leader>r :syntax sync fromstart<cr>
nnoremap <silent> <leader>c :nohlsearch<CR>
"quick buffer switching
nnoremap <leader>, :bn<cr>
nnoremap <leader>. :bp<cr>
"emmet generate html
imap <silent> <leader>ee <C-y>,
"go to next wrapped line, not the line after line end
"noremap j gj 
"noremap k gk
au BufEnter *.rb set sw=2 sts=2 et ai
au BufNewFile *.rb set fileformat=unix tabstop=2 shiftwidth=2 softtabstop=2
au BufEnter *.py set sw=4 sts=4 et ai
au BufNewFile *.py set fileformat=unix tabstop=4 shiftwidth=4 softtabstop=4
au BufEnter *.jsp set sw=2 sts=2 et ai
au BufNewFile *.jsp set fileformat=unix tabstop=2 shiftwidth=2 softtabstop=2
au BufEnter *.java set sw=2 sts=2 et ai
au BufNewFile *.java set fileformat=unix tabstop=2 shiftwidth=2 softtabstop=2
au BufEnter *.js set sw=4 sts=4 et ai
au BufNewFile *.js set fileformat=unix tabstop=4 shiftwidth=4 softtabstop=4
au BufEnter *.html set sw=4 sts=4 et ai
au BufNewFile *.html set fileformat=unix tabstop=4 shiftwidth=4 softtabstop=4
au BufEnter *.php set sw=4 sts=4 et ai
au BufNewFile *.php set fileformat=unix tabstop=4 shiftwidth=4 softtabstop=4

" Go to last file if invoked without arguments.
autocmd VimEnter * nested if
  \ argc() == 0 &&
  \ bufname("%") == "" &&
  \ bufname("2" + 0) != "" |
  \   exe "normal! `0" |
  \ endif

" When editing a file, always jump to the last known cursor position.
" Don't do it when the position is invalid or when inside an event handler
" (happens when dropping a file on gvim).
autocmd BufReadPost *
	\ if line("'\"") > 0 && line("'\"") <= line("$") |
	\   exe "normal g`\"" |
	\ endif

func! WordProcessorMode() 
  setlocal formatoptions=1 
  setlocal noexpandtab 
  noremap j gj 
  noremap k gk
  "setlocal spell spelllang=en_us 
  "set thesaurus+=/Users/sbrown/.vim/thesaurus/mthesaur.txt
  set complete+=s
  set formatprg=par
  setlocal wrap 
  setlocal linebreak 
endfu 

com! WP call WordProcessorMode()

func! Split4Win() 
    :vsplit
    :split
    :wincmd l
    :split
    :wincmd h
endfu 

com! Sp4 call Split4Win()

au VimEnter * RainbowParenthesesToggle
au syntax * RainbowParenthesesLoadRound
au syntax * RainbowParenthesesLoadSquare
au syntax * RainbowParenthesesLoadBraces

let g:rbpt_colorpairs = [
    \ ['brown',       'RoyalBlue3'],
    \ ['Darkblue',    'SeaGreen3'],
    \ ['darkgray',    'DarkOrchid3'],
    \ ['darkgreen',   'firebrick3'],
    \ ['darkcyan',    'RoyalBlue3'],
    \ ['darkred',     'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['brown',       'firebrick3'],
    \ ['gray',        'RoyalBlue3'],
    \ ['black',       'SeaGreen3'],
    \ ['darkmagenta', 'DarkOrchid3'],
    \ ['Darkblue',    'firebrick3'],
    \ ['darkgreen',   'RoyalBlue3'],
    \ ['darkcyan',    'SeaGreen3'],
    \ ['darkred',     'DarkOrchid3'],
    \ ['red',         'firebrick3'],
    \ ]

let g:rbpt_max = 16
let g:rbpt_loadcmd_toggle = 0
