
# Aliases
#
#

alias rm='rm -i'
alias ll='ls -lArth | tail'
alias lsd='ls -lardt ./.??*'
#alias vm='vim -X -T xterm-color'
alias vm='nvim'
alias nvm='nvim'
alias cdb='cd /home/zenshade/Dropbox'
alias zd='cd /home/zenshade/zendata'
alias ccl='cd /home/zenshade/zendata/clj-projects'
alias dls='cd /home/zenshade/Downloads'
alias pywebs='python -m SimpleHTTPServer'

# digital ocean ubuntu image at cyberzen.net
alias czftp='sftp -oPort=5126 zenshade@104.131.176.104'
alias scz='ssh -p 5126 zenshade@104.131.176.104'


#L&M connections
alias rath='ssh -l root 107.135.136.41'
alias rher='ssh -l root 107.135.136.42'

alias cdev='cd /home/zenshade/zendata/dev'
alias clims='cd /home/zenshade/zendata/dev/source/lims'
alias dpl='~/bin/dep-lims.sh'

#git stuff
alias g.s='git status'
alias g.a='git add'
alias g.c='git commit'
alias g.p='git push'
alias g.ac='git add .; git commit'
alias g.cap='git add .; git commit; git push'

alias ncode='java -jar /home/zenshade/zendata/clj-projects/nightcode-1.3.0-standalone.jar'
alias nmod='java -jar /home/zenshade/zendata/clj-projects/nightmod-1.3.0-standalone.jar'

